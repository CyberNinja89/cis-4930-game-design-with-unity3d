﻿//***************
// Jeffrey Kuang
// SkellyController.cs
// 4/2/2015
// This script defines the behavior 
// a prefab skeleton
//***************

using UnityEngine;
using System.Collections;

public class SkellyController: MonoBehaviour 
{
	// Public variables to attach
	public TextMesh word;
	public GameObject head;

	// Private variables to be accessed within the script
	private string word1, word2;
	private GameObject target;
	private AnimatorStateInfo state;
	private Animator skellyAnim;
	private bool attacked;
	private Vector3 startPosition, position, wordPosition;

	// Initialization Variables
	void Start () {
		attacked = false;
		// Initially start looking at the target
		target = GameObject.Find("Target");
		transform.LookAt (target.transform.position);
		// Generate the words
		setWords ();
		skellyAnim = GetComponent<Animator>();
		// set and store the randomized position
		transform.position = startPosition = getPosition();
	}
	
	// Update is called once per frame
	void Update() {
		// Calculate the distance from target, to help define a sense of range from target
		float distance = Vector3.Distance(transform.position,target.transform.position);
		transform.LookAt (target.transform.position);

		// Obtain the current animation state
		state = skellyAnim.GetCurrentAnimatorStateInfo(0);

	    // Initial walking state
		if(state.nameHash == Animator.StringToHash("SkellyState.Walking")){
			// Set the first word and show the text abve the skeleton
			word.text = word1;
			word.gameObject.SetActive(true);
			word.gameObject.transform.position = head.transform.position + new Vector3(0.0f,2.5f,0.0f);

			// "Move" the skeleton like it's walking
			transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime);
		}

		// Post attack walking state
		if(state.nameHash == Animator.StringToHash("SkellyState.Walking 1")){
			// After walking off camera, trigger restarting state and preparation
			if (distance < 0.5){
				word.gameObject.SetActive(false);
				skellyAnim.SetTrigger ("WaitTrigger");
				// get new words
				setWords ();
				// new random position
				startPosition = getPosition();
			}
			attacked = false;
			transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime);
		}

		// Mid-Attack State
		if (state.nameHash == Animator.StringToHash ("SkellyState.Attack 1")) {
			attacked = true;
			// disable success typing after it disappears
			word.text = "    "; 
			word.gameObject.SetActive(false);
		}

		// Trigger the attack animation state
		if (distance < 2 && !attacked && state.nameHash == Animator.StringToHash("SkellyState.Walking")) {
			skellyAnim.SetTrigger ("AttackTrigger");
		}

		// Crawling State after first damage
		if(state.nameHash == Animator.StringToHash("SkellyState.Crawling")){
			// Once off camera trigger the restarting state and prepartion
			if (distance < 1){
				word.gameObject.SetActive(false);
				skellyAnim.SetTrigger ("WaitTrigger");
				// get new words
				setWords ();
				// new random position
				startPosition = getPosition();
			}
			// otherwise set the 2nd word and show the text above the skeleton on the floor
			word.text = word2;
			word.gameObject.SetActive(true);
			word.gameObject.transform.position = head.transform.position + new Vector3(0.0f,1.0f,0.0f);

			// "Move" the skeleton like it's crawling at half the speed of walking
			transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime/2);
		}

		// Death state after being shot crawling
		if(state.nameHash == Animator.StringToHash("SkellyState.Death")){
			// disable word
			word.gameObject.SetActive(false);
			// sink the skeleton into the ground
			transform.position = Vector3.MoveTowards(transform.position, new Vector3(0,-2,0), Time.deltaTime);
			skellyAnim.SetTrigger ("WaitTrigger");
			// get new words
			setWords ();
			// new random position
			startPosition = getPosition();
		}

		// limbo state as it preps to respawn
		if(state.nameHash == Animator.StringToHash("SkellyState.Wait")){
			word.gameObject.SetActive(false);
			// place skeleton at the randomly picked location
			transform.position = startPosition;
		}

		// intermediate state immediately getting shot
		if(state.nameHash == Animator.StringToHash("SkellyState.Damage")){
			// briefly disable the word as it will generate a new word in next state
			word.gameObject.SetActive(false);
		}
	}

	// function to generate the list of words
	string generateWord() {
		string[] wordy = new string[] {"harasee","analyze","cognizant","mayonnaise","spaceship", "souvenir", "hostage", "olympics", "briefcase", "horseshoe", "arithmetic", "paradise", "catastrophe", "parlour", "doctrine", "rhinoceros"};
		string skellyWord = wordy[Random.Range (0, 16)];
		return skellyWord;
	}

	// set function to assign to variables
	void setWords(){
		word1 = generateWord ();
		word2 = generateWord ();
	}

	// function to generate the 3 possible positions
	Vector3 getPosition(){
		Vector3[] startPosition = new Vector3[] {new Vector3(-3.673097f, -0.16f, -2.13f),new Vector3(0.02674393f, -0.16f, -2.13f),new Vector3(3.990783f, -0.16f, -2.13f)};
		position = startPosition[Random.Range (0,3)];
		return position;
	}
}