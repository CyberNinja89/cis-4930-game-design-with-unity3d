﻿//***************
// Jeffrey Kuang
// GameController.cs
// 4/2/2015
// This script defines the behavior 
// a game and typing
//***************

using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	// Public Variable to attach to script
	public Animator skeletonPrefab1;
	public Animator skeletonPrefab2;
	public Animator skeletonPrefab3;
	public GUIText word;
	public TextMesh skellyWord1;
	public TextMesh skellyWord2;
	public TextMesh skellyWord3;
	public AudioClip pistol;

	// Private Variables
	private Vector3 startPosition;
	private string keyPressed;
	private string finalString;

	// Use this for initialization
	void Start () {
		finalString = "";
		// disable the other skeletons
		skeletonPrefab1.gameObject.SetActive (false);
		skeletonPrefab3.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		// delate the first skeleton from spawning
		if ( Time.time > 8.0f && Time.time < 9.0f)
			skeletonPrefab1.gameObject.SetActive (true);

		// delay the second skeleton from spawning
		if ( Time.time > 17.0f && Time.time < 18.0f)
			skeletonPrefab3.gameObject.SetActive (true);

		// If the inputted string matches the first skeleton
		if (finalString == skellyWord1.text){
			Debug.Log (skellyWord1.text);
			// reset the stored string
			finalString = "";
			// play audio and trigger damaged states
			audio.PlayOneShot(pistol, 1);
			skeletonPrefab1.SetTrigger ("DamageTrigger");
		}

		// If the inputted string matches the second skeleton
		if (finalString == skellyWord2.text){
			Debug.Log (skellyWord2.text);
			// reset the stored string
			finalString = "";
			// play audio and trigger damaged states
			audio.PlayOneShot(pistol, 1);
			skeletonPrefab2.SetTrigger ("DamageTrigger");
		}

		// If the inputted string matches the third skeleton
		if (finalString == skellyWord3.text){
			Debug.Log (skellyWord3.text);
			// reset the stored string
			finalString = "";
			// play audio and trigger damaged states
			audio.PlayOneShot(pistol, 1);
			skeletonPrefab3.SetTrigger ("DamageTrigger");
		}
		foreach (char c in Input.inputString) {
			if (Input.GetKey ("return") || Input.GetKey ("space") || Input.GetKey (KeyCode.KeypadEnter)){
				// store the word after it has entered a string
				finalString = word.text;
				word.text = "";
			} 
			else
				word.text += c;
		}
	}
}
