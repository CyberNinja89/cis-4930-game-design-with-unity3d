********** Typing Dead **********
Name:
  Jeffrey Kuang

Date:
  4/2/2015

Description:
    This project focuses on manipulating animation states. This game features skeletons that
contains words on the top of there heads. The goal of this game is to correctly type the words
in order to shoot the skeletons. To control the animations I created a pretty explicit set of
state machines to should encompass every possible situation within the game. To add a little bit,
I had the skeleton use all the attack animations. I also disable the ability to damage a skeleton
once it has entered the attack states. I reduced speed of the skeleton when it's crawling by half.
To get the skeletons look at a specific direction, I simply created a sphere gameObject it would
move towards or lookat.


Note: I attempted the extra credit which calls for having multiple skeletons. I delayed the
spawning by seconds to prevent the event of overlapping skeletons rushing towards the camera.