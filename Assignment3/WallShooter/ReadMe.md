********** WallShooter **********
Name:
  Jeffrey Kuang

Date:
  1/17/2015

Description:
  This project is a simple game of throwing a ball at a wall of colored blocks. I created a prefab
wall that resembles a bullseye target. The first wall spawns in front of the camera to give the player
a chance to test throw the ball at the wall a couple times and then disappears after 5 seconds. A
new wall will spawn at a random location after 2 seconds. The player will then move to break down the
new wall. When the ball impacts a block, the block will then change to white. I created a script called
HitMarker.cs to handle the color change upon impacted by the ball. I increased the mass of the prefab 
wall blocks to have a little more resistance against the ball. I increased the force of the thrown ball
so that it will travel faster and gravity wouldn't pull the ball down quickly. I have also added a little
light fixture above the wall to give the feel of an illuminated target in the distance. In addition to
accepting directional keys, it will also accept WASD keys for directions as well.