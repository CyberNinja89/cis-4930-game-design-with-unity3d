﻿//***************
// Jeffrey Kuang
// HitMarker.cs
// 1/17/2015
// This script defines the event of an
// collision and changes the block's color
//***************

using UnityEngine;

public class HitMarker : MonoBehaviour {	
	// Collision detecting function for each block in the wall
	void OnCollisionEnter(Collision obj) {
		// limit the effect of collision only if the ball touches the block
		if (obj.gameObject.name == "Ball(Clone)") { 
			Debug.Log ("collision from ball detected");
			// Change block to white
			renderer.material.color = Color.white;
		}
	}
}
