﻿//***************
// Jeffrey Kuang
// Shooter.cs
// 1/17/2015
// This script defines the movement of the
// camera and to fire the ball at the wall
//***************

using UnityEngine;

public class Shooter : MonoBehaviour {

	// Initializing GameObject objects
	private Vector3 cameraLocation;
	public GameObject ballPrefab;

	// Update is called once per frame
	void Update () {
		// Instantiates a ball object when space is pressed
		if (Input.GetKeyDown (KeyCode.Space)){
			// Store the transform of the camera's position
			cameraLocation = transform.localPosition;
			Debug.Log ("space key was pressed");
			// Instantiate the ball at the location of the camera
			GameObject ball = (GameObject)Instantiate(ballPrefab, cameraLocation, Quaternion.identity);
			// Apply Z-axis directional force by 1750 to simulate a fast thrown ball
			ball.rigidbody.AddForce(Vector3.forward * 1750);
			Debug.Log ("ball was thrown");
			// The ball lasts for 2 seconds
			Destroy (ball, 2.0f);
		}
		// GetKey will keep the camera moving when the key is held down
		else if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W)){
			Debug.Log ("up key was pressed");
			// translating the object by 0.5 for smoother movement
			transform.Translate(new Vector3(0,0,0.5f));
		}
		else if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S)){
			Debug.Log ("down key was pressed");
			transform.Translate(new Vector3(0,0,-0.5f));
		}
		else if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)){
			Debug.Log ("right key was pressed");
			transform.Translate(new Vector3(0.5f,0,0));
		}
		else if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)){
			Debug.Log ("left key was pressed");
			transform.Translate(new Vector3(-0.5f,0,0));
		}
	}
}
