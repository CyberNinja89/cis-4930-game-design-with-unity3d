﻿//***************
// Jeffrey Kuang
// WallGenerator.cs
// 1/17/2015
// This script instantiates a prefabriate wall
// of blocks at random locations
//***************

using UnityEngine;

public class WallGenerator : MonoBehaviour {

	// Initializing the GameObject and time variable
	public GameObject wallPrefab;
	private float running_time = 0.0f;

	void Start () {
		// Instantiate the first wall in front of the camera
		GameObject wall = (GameObject)Instantiate(wallPrefab, new Vector3(0, 0, 0), Quaternion.identity);
		// The wall only lives for 5 seconds
		Destroy (wall, 5.0f);
	}

	void Update() {
		// keep adding the difference of time between frames
		running_time += Time.deltaTime;

		// Once the time reaches 7 seconds, re-instantiate a new wall
		if (running_time > 7){
			// Randomly generate a float
			float randx = Random.Range(0,30);
			float randz = Random.Range(0,30);
			// Instantiate the reandomly located prefab wall
			GameObject wall = (GameObject)Instantiate(wallPrefab, new Vector3(-15.0f + randx, 0.0f, 0.0f + randz), Quaternion.identity);
			// remove the wall after 5 seconds
			Destroy (wall, 5.0f);
			// remove 7 seconds to refreash the running time
			running_time -= 7;
		}
	}
}