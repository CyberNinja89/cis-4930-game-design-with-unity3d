********** CarShow **********
Name:
  Jeffrey Kuang

Date:
  1/26/2015

Description:
  This project is a demostration of interscript calls and movement through linear interpolation. Switch 1
enables/disables the spotlight. Switch 2 controls the rotation of the podium. Switch 3 raises/lowers the 
podium with the car resting on it. I placed the stockcar under the heirarchy of the podium, because I was
having difficulty letting the physics grip the podium. Without the heirarchy the stockcar is barely turning
as fast as the podium. I attempted to change drag and mass but it don't appear to affect the rate of rotation.

The Podium script partially follows the code used in the linear interpolation Official Unity demo. I used the
existing sample switch interaction script as the basis for control the remain two switchs. Then ultimately I
used the provided prefab switch script and didn't have to modify any of the exisiting functions. I used the
prefabricated switches and car in this project.

NOTE: I went for the extra credit. Based upon my understanding of the writeup, I understood the directions as
"rotate the podium one full rotation from where the podium has currently stopped" before shutting the switch
off.