﻿//***************
// Jeffrey Kuang
// Switch.cs
// 1/25/2015
// This class defines the all the 
// switch funtionalities.
// This script was fully provided
// from InterScriptSample folder
//***************

using UnityEngine;
using System;
using System.Collections;

public class Switch : MonoBehaviour 
{
	// Declare public GameObjects, SwitchState
	public GameObject thumb, light;
	public enum SwitchState {ON = -22, OFF = 22};
	public SwitchState state;

	// Start SwtichState as off
	void Start () 
	{
		state = SwitchState.OFF;
	}

	// Check on the Switch State at each frame update
	void Update()
	{
		setSwitchState(state);
	}

	// Function to change state to the current state
	void setSwitchState (SwitchState toSet)
	{
		state = toSet;
		// Flip the angle of the switch and change color
		thumb.transform.localEulerAngles = new Vector3((float)state, 0,90);
		light.renderer.material.color = (state == SwitchState.OFF) ? Color.red : Color.green;
	}

	// Function to Set the switch state
	public void set(SwitchState val)
	{
		state = val;
	}

	// Return the current switch state
	public SwitchState get()
	{
		return state;
	}

	// Function to set switch state and call an action
	public void setAndDo(SwitchState toSet, Action toDo)
	{
		/*
		 * TBD
		 */

		state = toSet;
		toDo();
	}

	// Function to flip to opposite switch state
	public static SwitchState getOppositeState(SwitchState s)
	{
		return (s == SwitchState.OFF) ? SwitchState.ON : SwitchState.OFF;
	}
}
