﻿//***************
// Jeffrey Kuang
// Podium.cs
// 1/26/2015
// This script defines the movements of
// the podium (lowering and rotation)
//***************

using UnityEngine;
using System.Collections;

public class Podium : MonoBehaviour 
{
	// Declare Public GameObjects and Vectors
	private GameObject switch2;
	private GameObject switch3;
	private Vector3 newPosition;
	private Vector3 currentRotation;
	private Vector3 fullRotation;
	
	// This is called at start of the of script 
	void Start () 
	{
		// temporarily store the starting position
		newPosition = transform.position;
		currentRotation = new Vector3 (0, 0, 0);
		fullRotation = new Vector3(0, 360, 0);
	}

	// Call these functions at each frame change
	void Update () 
	{
		ChangePosition ();
		RotatePosition ();
	}

	// Vertical Linear Interpolation of the podium
	void ChangePosition ()
	{
		// Associates GameObject Switch to variable
		switch3 = GameObject.Find("Switch3");

		// Associate the Switch3 script to provide access
		Switch script3 = switch3.GetComponent<Switch> ();

		// Declaring podium positions at raised and lowered positions
		Vector3 RaisedPosition = new Vector3 (0,0.50f,-0.25f);
		Vector3 LoweredPosition = new Vector3 (0,0.175f,-0.25f);

		// Define target position based on switch state
		if (script3.get().ToString() == "ON")
			newPosition = LoweredPosition;
		if (script3.get().ToString() == "OFF")
			newPosition = RaisedPosition;

		// move GameObject to the target vector
		transform.position = Vector3.Lerp (transform.position, newPosition, Time.deltaTime/1.5f);
	}

	// Rotation of the podium which the vehicle rests on
	void RotatePosition ()
	{
		// Associates GameObject Switch to variable
		switch2 = GameObject.Find ("Switch2");

		// Associate the Switch2 script to provide access
		Switch script2 = switch2.GetComponent<Switch> ();

		// Start Rotation by moving 35 degrees per frame
		if (script2.get().ToString() == "ON")
		{
			transform.Rotate(Vector3.up * Time.deltaTime * 35);
			currentRotation += Vector3.up * Time.deltaTime * 35;
			//Debug.Log ("Currently at "+ newRotation);
		}

		// Stop rotation once it reaches a full rotation and store postion
		if (currentRotation.magnitude > fullRotation.magnitude)
		{
			script2.set (Switch.SwitchState.OFF);
			fullRotation = currentRotation;
			Debug.Log ("Completed full rotation");
		}

		// When switched off calculate the next full rotation from current
		if (script2.get().ToString() == "OFF")
		{
			fullRotation = currentRotation + new Vector3(0, 360, 0);
			//Debug.Log ("This will stop at "+ fullRotation);
		}
	
	}
}
