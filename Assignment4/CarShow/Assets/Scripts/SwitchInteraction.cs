﻿//***************
// Jeffrey Kuang
// SwitchInteraction.cs
// 1/25/2015
// This script defines the interaction
// of the switches when A/S/D is pressed.
// This script was partially provided
// from InterScriptSample folder
//***************

using UnityEngine;
using System.Collections;

public class SwitchInteraction : MonoBehaviour 
{
	// Declare Public Switches
	public Switch switch1;
	public Switch switch2;
	public Switch switch3;

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.A)){
			// Get and assign the opposite switch state to toSet function
			Switch.SwitchState toSet = Switch.getOppositeState(switch1.get());
			switch1.set(toSet);
			Debug.Log ("Flipping Switch 1");
		}
		else if(Input.GetKeyUp(KeyCode.S)){
			Switch.SwitchState toSet = Switch.getOppositeState(switch2.get());
			switch2.set(toSet);
			Debug.Log ("Flipping Switch 2");
		}
		else if(Input.GetKeyUp(KeyCode.D)){
			Switch.SwitchState toSet = Switch.getOppositeState(switch3.get());
			switch3.set(toSet);
			Debug.Log ("Flipping Switch 3");
		}
	}
}
