﻿//***************
// Jeffrey Kuang
// Light.cs
// 1/25/2015
// This script defines the lighting
// activity of the spotlight
//***************

using UnityEngine;
using System.Collections;

public class Light : MonoBehaviour 
{
	// Declare Public GameObject
	private GameObject switch1;
		
	// Check lighting state on each update
	void Update () 
	{
		enableLight ();
	}

	// Checks switch state and turns on/off spotlight
	void enableLight() 
	{
		// Associates GameObject Switch to variable
		switch1 = GameObject.Find ("Switch1");

		// Associate the Switch script to provide access
		Switch script = switch1.GetComponent<Switch> ();

		// Turn on/off based on script's switch state  
		if (script.get().ToString() == "ON")
			light.enabled = true;

		if (script.get().ToString() == "OFF")
			light.enabled = false;
	}
}
