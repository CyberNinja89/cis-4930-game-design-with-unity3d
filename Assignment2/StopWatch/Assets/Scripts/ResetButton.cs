﻿//***************
// Jeffrey Kuang
// ResetButton.cs
// 1/12/2015
// This script defines the movement of the
// reset button on the stopwatch
//***************

using UnityEngine;

public class ResetButton : MonoBehaviour 
{
	// Initializing the duration of the object movement
	private float resetMotion = 0.0f;
	
	void Update () 
	{
		// "Presses" the reset button into the stopwatch
		if (Time.time < (resetMotion - .25f))
			// Using Vector3.down because the object y-axis is point out of the stopwatch
			transform.Translate (Vector3.down * Time.deltaTime / 10);
		// "Releasing" the reset button
		else if (Time.time < resetMotion)
			transform.Translate (Vector3.up * Time.deltaTime / 10);
		// Creating the duration of object movement when Escape is pressed
		else if(Input.GetKeyDown (KeyCode.Escape))
		    resetMotion = Time.time + .5f;
	}
}
