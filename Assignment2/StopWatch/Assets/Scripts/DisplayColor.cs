﻿//***************
// Jeffrey Kuang
// DisplayColor.cs
// 1/12/2015
// This script defines the color changes of the
// outer rim of the stopwatch
//***************

using UnityEngine;

public class DisplayColor : MonoBehaviour 
{
	// Initializing string variable representing the current color state
	private string status = "red";
	 
	void Update () 
	{
		// When start/stop button has been pressed
		if (Input.GetKeyDown (KeyCode.Space))
		{
			// Color change to green when stopwatch starts
			if (status == "red")
			{
				Debug.Log("space key was pressed - Start");
				renderer.material.color = Color.green;
				status = "green";
			}
			// Color change to red when stopwatch stops
			else if (status == "green")
			{
				Debug.Log("space key was pressed - Stop");
				renderer.material.color = Color.red;
				status = "red";
			}
		}
		// When reset button has been pressed, change color to red
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Debug.Log("escape key was pressed - Reset");
			renderer.material.color = Color.red;
			status = "red";
		}
	}
}
