﻿//***************
// Jeffrey Kuang
// MinuteRotate.cs
// 1/12/2015
// This script defines the movement of the
// minutes hand on the stopwatch
//***************

using UnityEngine;

public class MinuteRotate : MonoBehaviour 
{
	// Initialization of placeholding variables
	private float timeRunning = 0.0f;
	private float timeCounter = 0.0f;	// For debugging minutes
	private bool running = false;
	
	void Update () 
	{
		// Resets the position, seconds counter, and state of the stopwatch
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			transform.rotation = Quaternion.identity;
			timeCounter = 0.0f;
			running = false;
		}
		if (Input.GetKeyDown (KeyCode.Space))
		{
			// Starts the stopwatch
			if (running == false)
			{
				// Add the change in time between frames
				timeRunning += Time.deltaTime;
				timeCounter += Time.deltaTime;
				// change state
				running = true;
			}
			// Stops the stopwatch
			else if (running == true)
			{
				running = false;
				// Outputs to console the minutes the stopwatch should be showing
				Debug.Log ("Minutes = " + (timeCounter/60).ToString("0.0"));
			}
		}
		// Move the seconds hand when deltaTime has been more than 1 second
		if (timeRunning > 1)
		{
			// Since z-axis is point into the screen a counter-clockwise degree
			// notation is need to mirror the appropriate direction. Each minute
			// should be -0.1 degrees
			transform.Rotate (new Vector3(0, 0, -0.1f));
			timeRunning = 0;
		}
		// Keep adding deltaTime while stopwatch is running
		if (running == true)
		{
			timeRunning += Time.deltaTime;
			timeCounter += Time.deltaTime;
		}
	}
}