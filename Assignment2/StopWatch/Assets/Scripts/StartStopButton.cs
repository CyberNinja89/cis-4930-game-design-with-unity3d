﻿//***************
// Jeffrey Kuang
// StartStopButton.cs
// 1/12/2015
// This script defines the movement of the
// start/stop button on the stopwatch
//***************

using UnityEngine;

public class StartStopButton : MonoBehaviour 
{
	// Initializing the duration of the object movement
	private float startStopMotion = 0.0f;
	
	void Update () 
	{
		// "Presses" the start/stop button into the stopwatch
		if (Time.time < (startStopMotion - .25f))
			// Using Vector3.down because the object y-axis is point out of the stopwatch
			transform.Translate (Vector3.down * Time.deltaTime / 10);
		// "Releasing" the start/stop button
		else if (Time.time < startStopMotion)
			transform.Translate (Vector3.up * Time.deltaTime / 10);
		// Creating the duration of object movement when Space is pressed
		else if(Input.GetKeyDown (KeyCode.Space))
			startStopMotion = Time.time + .5f;
	}
}
