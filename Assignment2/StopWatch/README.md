********** StopWatch **********
Name:
  Jeffrey Kuang

Date:
  1/12/2015

Description:
  This project simulates the actions of a physical stopwatch. This stopwatch project primarily 
features an accurate moving minute and seconds hand that acts just like an analog stopwatch. 
Also as an indicator, the outer rim changes color to signify when the stopwatch is running or 
it has stopped. In addition to the minimum requirements of this project, I have also included a
moving start/stop and reset button to simulate the actions of a stopwatch. For debugging purposes,
the script logs the minutes and seconds that the watch face should represent along with keyboard
interactions to the console.