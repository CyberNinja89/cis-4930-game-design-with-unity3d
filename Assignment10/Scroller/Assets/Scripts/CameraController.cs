﻿//***************
// Jeffrey Kuang
// CameraController.cs
// 4/9/2015
// This script defines the movement of the
// camera focusing on the character on the
// x-axis only
//***************

using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	// Variable Declarations
	public GameObject character;
	private float offset;

	// Use this for initialization
	void Start () {
		// storing the offset distance from character
		offset = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		// Define a new position for the camera as an x-axial offset from the character plus the character translation
		transform.position = new Vector3(character.transform.position.x + offset, transform.position.y, transform.position.z);
	}
}
