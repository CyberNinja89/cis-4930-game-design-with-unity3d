﻿//***************
// Jeffrey Kuang
// Background.cs
// 4/9/2015
// This script defines the behavior of the
// Background image
//***************

using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	// Public Variables
	public new GameObject camera;
	public float speed = 0;
		
	// Update is called once per frame
	void Update () {

		// The original line of code came from the tutorial by Mike Geig, the man who does live Unity demos 
		// I add swapped out the time factor so it doesn't continuously scroll through the background
		// now it uses the camera's position as a point of reference in terms of movement.
		renderer.material.mainTextureOffset = new Vector2(camera.transform.position.x * (speed/4), 0f);
	}
}
