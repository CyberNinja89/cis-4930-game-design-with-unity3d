﻿//***************
// Jeffrey Kuang
// FoxController.cs
// 4/9/2015
// This script defines the behavior of the
// enemy character
//***************

using UnityEngine;
using System.Collections;

public class FoxController : MonoBehaviour {

	// Public Variable Declaration
	public GameObject target;
	public float speed = 2.0f;
	public float LeftLimit = 0.0f;
	public float RightLimit = 5.0f;
	public GameObject projectilePrefab;
	public AudioClip fire;

	// Private Variable Declaration
	bool facingRight = true;
	float direction = 1.0f;
	Vector3 Amount;
	float interval;

	// Declaring GameObject Components
	Animator anim;
	BoxCollider2D boxcollider;
	CircleCollider2D circlecollider;

	// Use this for initialization
	void Start () {
		// Assign Components to a variable for later access
		anim = GetComponent<Animator> ();
		boxcollider = GetComponent<BoxCollider2D>();
		circlecollider = GetComponent<CircleCollider2D>();
	}
	// Update is called once per frame
	void Update () {
		// Calculate the distance of the fox to the target
		float distance = Vector3.Distance (transform.position, target.transform.position);

		// timer interval to control how often the foxes throw nuts
		interval += Time.deltaTime;
		if (interval >= 1.0f){ 
			// throwing in the right direction  by instantiating the projectile
			if (facingRight && distance <= 5.0f) {
				GameObject nuts = (GameObject)Instantiate (projectilePrefab, transform.position + new Vector3 (0.45f, 0, 0), Quaternion.identity);
				audio.PlayOneShot (fire, 1.0f);
				nuts.rigidbody2D.AddForce (new Vector2 (1, 0) * 800);
				Destroy (nuts, .5f);
			} else if (!facingRight && distance <= 5.0f) {
				// throwing in the left direction by instantiating the projectile
				GameObject nuts = (GameObject)Instantiate (projectilePrefab, transform.position + new Vector3 (-0.45f, 0, 0), Quaternion.identity);
				audio.PlayOneShot (fire, 1.0f);
				nuts.rigidbody2D.AddForce (new Vector2 (-1, 0) * 800);
				Destroy (nuts, .5f);
			}
			interval -= 1.0f;
		}

		// control flow for the foxes to patrol
		// get the distance walked
		Amount.x = direction * speed * Time.deltaTime;
		// turn left once it hits the right side
		if (direction > 0.0f && transform.position.x >= RightLimit){
			direction = -1.0f;
			Flip ();
		}
		// turn right once it hits the left side
		else if (direction < 0.0f && transform.position.x <= LeftLimit){
			direction = 1.0f;
			Flip ();
		}
		// move the foxes the distance it traveled
		transform.Translate(Amount);
	}

	// controls the presentation of the sprite if it turns opposite direction
	void Flip()	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnCollisionEnter2D(Collision2D obj) {
		// If the fox gets hit by the nuts it instantiously dies
		if (obj.gameObject.tag == "Projectile") { 
			// trigger the death animation for the fox
			anim.SetTrigger ("Death");
			// It falls through the floor and gets destroyed
			boxcollider.enabled = false;
			circlecollider.enabled = false;
			Destroy (this.gameObject, 1.0f);
		}
	}
}
