//***************
// Jeffrey Kuang
// CharController.cs
// 4/9/2015
// This script defines the behavior of the
// character
//***************

using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {

	// Public Variable Declaration
	public AudioClip jump, fire, loss, death;
	public Transform groundCheck;
	public GameObject projectilePrefab;
	public float maxSpeed = 10f;
	public LayerMask whatIsGround;
	public float jumpForce = 700f;

	// Private Variable Declaration
	float groundRadius = 0.2f;
	bool facingRight = true;
	bool grounded = false;
	bool doubleJump = false;
	float health = 3;

	// Declaring GameObject Components
	Animator anim;
	BoxCollider2D boxcollider;
	CircleCollider2D circlecollider;

	// Use this for initialization
	void Start () {
		// Assign Components to a variable for later access
		anim = GetComponent<Animator> ();
		boxcollider = GetComponent<BoxCollider2D>();
		circlecollider = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		// Check if the character is on the ground
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool ("Ground", grounded);

		// Flag to enable a secondary jump
		if (grounded)
			doubleJump = false;
		//if(!grounded) return;

		// setting float value to the animator parameters for jumping animations
		anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);

		// Get axial input from user
		float move = Input.GetAxis ("Horizontal");

		// setting float value for movement
		anim.SetFloat ("Speed", Mathf.Abs (move));
		rigidbody2D.velocity = new Vector2 (move * maxSpeed, rigidbody2D.velocity.y);

		// Control the direction the character is facing
		if (move > 0 && !facingRight)
			Flip ();
		else if (move < 0 && facingRight)
			Flip ();
	}

	void Update()
	{
		// FIRE THEM NUTS!!! 
		if (Input.GetKeyDown (KeyCode.Return)){
			// flow control for firing in the right direction and disappears after half a second
			if (facingRight){
				GameObject nuts = (GameObject)Instantiate(projectilePrefab, transform.position + new Vector3(0.45f, 0, 0),Quaternion.identity);
				audio.PlayOneShot (fire,1.0f);
				nuts.rigidbody2D.AddForce(new Vector2(1,0) * 1000);
				Destroy (nuts, .5f);
			} else {
				// flow control for firing in the left direction and disappears after half a second
				GameObject nuts = (GameObject)Instantiate(projectilePrefab, transform.position + new Vector3(-0.45f, 0, 0), Quaternion.identity);
				audio.PlayOneShot (fire,1.0f);
				nuts.rigidbody2D.AddForce(new Vector2(-1,0) * 1000);
				Destroy (nuts, .5f);
			}
		}

		// Flow control to check if character can start jumping
		if ((grounded || !doubleJump) && Input.GetKeyDown (KeyCode.Space)) {
			audio.PlayOneShot (jump,1.0f);
			anim.SetBool ("Ground", false);
			rigidbody2D.AddForce(new Vector2 (0, jumpForce));

			// DOUBLE JUMP!!! press in rapid succession to get higher jump!
			if (!doubleJump && !grounded){
				audio.PlayOneShot (jump,1.0f);
				doubleJump = true;
			}
		}

		// Death flow once health reaches zero
		if (health <= 0) {
			// trigger death animation
			anim.SetTrigger ("Death");
			// have the character fall through the floor
			boxcollider.enabled = false;
			circlecollider.enabled = false;
			audio.PlayOneShot (death, 1.0f);
			// Delay by a second so player realizes the end before restart
			StartCoroutine ("wait");
		}
	}

	IEnumerator wait()
	{
		// pause for 4 seconds
		yield return new WaitForSeconds (4.0f);
		//reload the same scene!
		Application.LoadLevel(Application.loadedLevel);
	}

	// controls the presentation of the sprite if it turns opposite direction
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		// Control for knockback and health deteriation by dangerous objects based upon the direction character is facing
		if (coll.gameObject.tag == "Traps") {
			if (facingRight){
				Debug.Log ("collision with trap detected");
				rigidbody2D.AddForce(new Vector2(-1200.0f,20.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			} else {
				Debug.Log ("collision with trap detected");
				rigidbody2D.AddForce(new Vector2(1200.0f,20.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			}
		}
		
		// Control for knockback and health deteriation by enemy contact based upon the direction character is facing
		if (coll.gameObject.tag == "Enemy") {
			if (facingRight){
				Debug.Log ("collision with the enemy detected");
				rigidbody2D.AddForce(new Vector2(-1000.0f,1.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			} else {
				Debug.Log ("collision with the enemy detected");
				rigidbody2D.AddForce(new Vector2(1000.0f,1.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			}
		}

		// Control for knockback and health deteriation by projectile based upon the direction character is facing
		if (coll.gameObject.tag == "Enemy Projectile") {
			if (facingRight){
				Debug.Log ("collision with the enemy projectile detected");
				rigidbody2D.AddForce(new Vector2(-1000.0f,1.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			} else {
				Debug.Log ("collision with the enemy projectile detected");
				rigidbody2D.AddForce(new Vector2(1000.0f,1.0f));
				health -= 1.0f;
				audio.PlayOneShot (loss,1.0f);
			}
		}
	}
}