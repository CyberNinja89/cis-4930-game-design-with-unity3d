********** Nuts! The Side Scroller **********
Name:
  Jeffrey Kuang

Date:
  4/9/2015

Description:
    This project focuses on manipulating 2D Mode and handling sprites with
custom animations. This idea behind this side scroller is this dog-like animal
fending off the hungery foxes at the park. Since it doesn't know how to comunicate 
with the foxes, the dog ultimately resorts to throwing nuts at the foxes.
The foxes also retaliate too with nuts! The protagonists has 3 hit points while
the foxes has one. Press enter to throw the nuts and space to jump. There is 
a special double jump in this game. The game resets if you die as well after 4
seconds. I used sound clips from old SNES/N64 games. Sprites were found on google
images and through the sprite database Dr. Small had provided. The some of the basic
features in the script were provided by the tutorial given by Mike Geig through a
pre-recorded Unity Demonstration for 2D side-scrollers.