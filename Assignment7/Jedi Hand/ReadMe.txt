********** Jedi Hand **********
Name:
  Jeffrey Kuang

Date:
  3/1/2015

Description:
  This project focused on ray casting and lookat features of Unity. I used one script that
controlled the movement of the hand to track the mouse position and send a directional force
upon the highlighted object. I only added a moderate randomized direction to object. Besides
the script I only tweaked a few things. I rotated the wrist into a relaxed pointing pose. 
I added one block to complete the triangle. I also changed the back wall into a cube with a 
deeper box collider instead of the plane. The reason was that I noticed the plane would 
sometimes not register a passing block with significant force from stopping it.