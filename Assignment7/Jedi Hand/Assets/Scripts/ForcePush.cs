﻿//***************
// Jeffrey Kuang
// ForcePush.cs
// 3/1/2015
// This script defines the behavior 
// of raycasted object and onclick event
//***************

using UnityEngine;
using System.Collections;

public class ForcePush : MonoBehaviour 
{
	// Public variables for specific materials
	public Material highlighted;
	public Material original;

	// Private variable to hold the previous highlighted gameobject
	private RaycastHit prior;

	// Update is called once per frame
	void Update () 
	{
		// Capture mouse position from the camera's perspective
		Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		Debug.DrawRay (mouseRay.origin, mouseRay.direction * 80, Color.blue, 0.1f);

		// Calculate the rotation distance from the mouse poistion from the arm
		Vector3 moveTo = mouseRay.GetPoint(60) - transform.position;

		// Creates a rotation to the mouse position
		Quaternion lookRotation = Quaternion.LookRotation (moveTo);

		// Move the arm in a smooth linear direction
		transform.rotation = Quaternion.Lerp (transform.rotation, lookRotation, Time.deltaTime * 100);

		RaycastHit current;

		// Boolean if statement for raycast hitting an object
		if (Physics.Raycast (mouseRay, out current, 80)) 
		{
			// Take action only on gameObjects named "Particle"
			if (current.collider.gameObject.name == "Particle") 
			{
				// If there was a previous raycasted object "Particle" change color back to original
				if (prior.collider)
					prior.collider.gameObject.renderer.material = original;

				//Debug.Log("Hit Object " + hit.collider.gameObject.name);
				// highlight raycasted object
				current.collider.gameObject.renderer.material = highlighted;

				if (Input.GetMouseButtonDown (0)) 
				{
					// Get and assign the opposite switch state to toSet function
					Debug.Log ("Clicky Clicky");
					Vector3 random = new Vector3( Random.Range(30.0F, 60.0F), Random.Range(10.0F, 20.0F), 0.0F);
					current.rigidbody.AddForce (moveTo + random * 400.0f);
				}
			
			// Store most recent RaycastHit object
			prior = current;
			} else {
				// If different gameObject "Particle" wasn't raycasted, that restore previous highlighted 
				// "Particle" if it exists
				if (prior.collider)
					prior.collider.gameObject.renderer.material = original;
			}
		}
	}
}