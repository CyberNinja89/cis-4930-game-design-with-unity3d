﻿//***************
// Jeffrey Kuang
// CameraController.cs
// 2/7/2015
// This script defines the behavior
// of the camera by following the
// ball
//***************

using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{
	// Public variable to attach player gameobject
	public GameObject player;

	// Private variable to instantiate
	private Vector3 offset;

	// Use this for initialization
	void Start () 
	{
		// Store the offset distance from player gameobject
		offset = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		// Move the camera to the new location
		transform.position = player.transform.position + offset;
	}
}
