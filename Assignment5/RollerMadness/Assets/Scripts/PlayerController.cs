﻿//***************
// Jeffrey Kuang
// PlayerController.cs
// 2/7/2015
// This script defines the behavior
// of the player/ball and the GUI
//***************

using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	// Public vairable to attach gameobjects
	public float speed;
	public GUIText countText;
	public GUIText levelText;
	public GUIText resultText;
	public GUIText timerText;

	// Private variable to instantiate
	private GenerateCoins script;
	private int count;
	private int level;
	private float bonus;
	private float time;

	// To be initialize at the beginning
	void Start()
	{
		level = 1;		// counter for level
		count = 0;		// counter for score
		time = 30;		// 30 second timer

		// Set the GUI text
		levelText.text = "Level " + level.ToString();
		resultText.text = "Welcome to Roller Madness: Infinite Edition!";
		SetCountText ();
		SetTimerText ();
	}

	// Update function based on physics movments
	void FixedUpdate()
	{
		// defining direction movements based on input keys
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		// Add direction force to the ball times public speed variable
		rigidbody.AddForce(movement * speed * Time.deltaTime);

		// Update the timer
		SetTimerText();
	}

	// Collision detection based on touch the coins with the ball
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Coin") 
		{
			// deactivate the objects and then destroy to create memory for new coins later
			other.gameObject.SetActive (false);
			Destroy (other.gameObject, 1.0f);

			// reset the message for next level
			resultText.text = "";

			// increment the counter after collision
			count++;
			Debug.Log("Coins: " + count.ToString());
			SetCountText ();

			// Check for level completetion
			CheckEndRound ();
		}
	}

	// Sets the counter GUI Text
	void SetCountText()
	{
		countText.text = "Count: " + count.ToString ();
	}

	// Sets the timer GUI Text
	void SetTimerText()
	{
		// keep descreasing the time
		if(time >= 0)
			time -= Time.deltaTime;
		else
		{
			// permanently set time to 0, display message,
			// and deactivate the player
			time = 0;
			resultText.text = "GAME OVER";
			Debug.Log("Game Over");
			GameObject.Find("Player").SetActive(false);
			return;
		}
		timerText.text = "Time: " + time.ToString ("0") + " sec";
	}

	//Check for level completetion
	void CheckEndRound()
	{
		// Checks it has collected 12 coins before proceeding
		if(count%12 == 0 && count != 0)
		{
			// Associate the GenerateCoins script to provide access
			script = GameObject.Find("Coins").GetComponent<GenerateCoins> ();

			// Notify and update GUI Texts
			resultText.text = "LEVEL " + level.ToString() + " COMPLETE";
			Debug.Log("Level Completed");
			level++;
			levelText.text = "Level " + level.ToString();

			// Bonus time gets smaller over time until after level 30
			if(level <= 30)
				bonus = 30 - level/2;
			else
				bonus = 15;
			Debug.Log("Adding " + bonus.ToString() + " seconds");
			time += bonus;

			// Generate more coins
			script.PlaceCoins();
		}
	}
}



