﻿//***************
// Jeffrey Kuang
// Rotator.cs
// 2/7/2015
// This script defines rotation
// of the coin
//***************

using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour 
{
	// Update fuction is called once per frame
	void Update () 
	{
		// This will rotate the coing along the Z-axis
		transform.Rotate (new Vector3 (0, 0, 135 * Time.deltaTime));
	}
}
