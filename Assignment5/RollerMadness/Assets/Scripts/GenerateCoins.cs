﻿//***************
// Jeffrey Kuang
// GenerateCoins.cs
// 2/7/2015
// This script defines the behavior
// of the generating randomly placed
// coins
//***************

using UnityEngine;
using System.Collections;

public class GenerateCoins : MonoBehaviour 
{
	// Public vairable to attach gameobjects
	public GameObject coinPrefab;

	// Use this for initialization
	void Start () 
	{
		PlaceCoins ();
	}

	public void PlaceCoins()
	{
		for(int i = 0; i < 12; i++)
		{
			// Randomly generate a float
			float randx = Random.Range(0,20);
			float randz = Random.Range(0,30);

			// Instantiate the reandomly located prefab coins and placed into the coins parent object
			GameObject coin = (GameObject)Instantiate (coinPrefab, new Vector3 (-10.0f + randx, 0.745f, -15.0f + randz), Quaternion.Euler (90, 0, 0));
			coin.transform.parent = GameObject.Find("Coins").transform;

		}
	}
}
