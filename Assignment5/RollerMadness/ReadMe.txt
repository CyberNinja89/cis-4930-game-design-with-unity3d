********** Rolling Madness: Infinite Edition **********
Name:
  Jeffrey Kuang

Date:
  2/7/2015

Description:
  This project is a recap and review of all the topics covered in this course. This project was based entirely 
from Untiy's beginner project example. In addition to the project I have added my flare of creativity upon
simplicity of this game. This games now features a level-like progress where the player race against time to collect
as many coins as possible. This is no end to the game but goal to obtain the highest score. The following are the
game concepts that I have implemented:

Randomly generated location of the coin for each level.
A timer that counts down to the end of the game.
A 30 second time bonus is rewarded for collecting all the coins in the level before the timer.
But the bonus time slowly shrinks, to provide a sense of level progression and difficulty.
At the end of the game, the player ball disappears to signify that the game is over.

FUN NOTE: This game is an homage to one of my childhood games, Sonic the Hedgehog.