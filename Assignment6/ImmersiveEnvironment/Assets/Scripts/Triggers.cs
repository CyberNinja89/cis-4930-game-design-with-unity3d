﻿//***************
// Jeffrey Kuang
// PlayerController.cs
// 2/15/2015
// This generic script defines the 
// behavior for a room and controls
// lighting and music
//***************

using UnityEngine;
using System.Collections;

public class Triggers : MonoBehaviour {

	// Declaring possible public gameobjects
	public GameObject colorLight1;
	public GameObject colorLight2;
	public GameObject colorLight3;
	public GameObject roomLights;
	public GameObject special;

	// Private variable to control music
	private int mode;
	private float volume;
	private float time;
	private int strobeCount;

	// Detection function when player enter a room
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Flynn")
		{
			Debug.Log ("Entering a room. Fade in Music");
			// Start audio if it's not playing
			if(!audio.isPlaying)
				audio.Play ();

			// mode 1 = play
			mode = 1;
			fadein();

			// Turn lights/objects on if GameObject is attached
			if (colorLight1 != null)
			{
				colorLight1.SetActive(true);
				colorLight2.SetActive(true);
				colorLight3.SetActive(true);
			}
			if (roomLights != null)
				roomLights.SetActive(true);
			if (special != null)
				special.SetActive(true);
		}
	}

	// Detection function when player exits a room
	void OnTriggerExit(Collider other){
		if(other.gameObject.tag == "Flynn")
		{
			Debug.Log ("Leaving a room. Fade out Music");
			// mode 1 = play
			mode = 0;
			fadeout();
			
			// Turn lights/objects off if GameObject is attached
			if (colorLight1 != null)
			{
				colorLight1.SetActive(false);
				colorLight2.SetActive(false);
				colorLight3.SetActive(false);
			}
			if (roomLights != null)
				roomLights.SetActive(false);
			if (special != null)
				special.SetActive(false);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (mode == 1 && volume < 1.0f)
			fadein ();
		else if (mode == 0 && volume > 0.0f)
			fadeout();
		if (colorLight1 != null && mode == 1)
			strobe();
	}

	void fadein ()
	{
		if (volume < 1.0f) 
		{
			// Gradually increase volume by delta time per frame
			volume += Time.deltaTime;
			audio.volume = volume;
		}
	}

	void fadeout ()
	{
		if (volume > 0.0f)
		{
			// Gradually decrease volume by delta time per frame
			volume -= Time.deltaTime;
			audio.volume = volume;
		}
	}

	void strobe ()
	{
		// This function controls the rate of strobing by seconds
		time += Time.deltaTime;
		if (time > 1.0f) 
		{
			// increment by 1 once it reaches past a full second
			strobeCount++;
			time -= 1.0f;
		}

		// Logic conditions for when the lights turn on or off
		if (strobeCount % 2 != 0)
			colorLight1.SetActive (true);
		else
			colorLight1.SetActive(false);
		
		if (strobeCount % 3 != 0)
			colorLight2.SetActive (true);
		else
			colorLight2.SetActive(false);

		if (strobeCount % 4 != 0)
			colorLight3.SetActive (true);
		else
			colorLight3.SetActive(false);
	}
}
