********** Kevin Flynn's Hideout **********
Name:
  Jeffrey Kuang

Date:
  2/15/2015

Description:
  This project focuses on the environmental aspects of a Unity scene. For this project. I created 
a two-story house that has 6 area/rooms. Every room has lighting and a song clipping attached to a room 
GameObject. I wrote one generic script that focuses on the trigger actions of a the player object. 
Music and lighting turns on when a player enters and turns off when the player leaves the room. The 
theme of this house was based on TRON : Legagcy. The first room is the living room that is supposed 
to make reference to Kevin Flynn's hideout. The room to the left is the garage that houses the two 
"Light" cars which I used from a previous homework assignment. I modified the texture to match the theme. 
The room the right is the game room that suppose to resemble an arena where Sam Flynn fought with his 
opponents. The opponent object was a generic third person object supplied from Unity Game Character 
Controller. I had also edited the texture of the character. Upstair, is a club themed room from the End
of Line Club in Tron. I implemented a strobe lighting effect with the color spotlights and I created a 
DJ-like booth to go with the dance floor. Outside of the club room on either side are balconies, with 
a platform and different song clips playing.