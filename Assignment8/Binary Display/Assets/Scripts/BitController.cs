﻿//***************
// Jeffrey Kuang
// BitController.cs
// 3/26/2015
// This script defines the behavior 
// of a single bit
//***************

using UnityEngine;
using System.Collections;

public class BitController : MonoBehaviour 
{
	// Public class variable delcarations
	public Material green;
	public Material red;
	public bool bitMoved;
	public TextMesh display;
	private Vector3 raisedPosition;
	private Vector3 loweredPosition;

	// Enumeration of the bit states for the binary display
	public enum GameState {off, on};

	// Instantiate the GameState
	public GameState currentState;

	// Used to initialize variables
	void Start () 
	{
		// Define the state and positions of on and off for lerping
		raisedPosition = transform.position + new Vector3 (0, 0.75f, 0);
		loweredPosition = transform.position;
		currentState = GameState.on;
	}

	// Update is called once per frame and checks the state
	void FixedUpdate () 
	{
		switch(currentState)
		{
			// repeatedily go to these states to update lerps
			case GameState.off:
				activate ();
				break;
			case GameState.on:
				deactivate ();
				break;
		}
	}

	// This function flips the state to enable movement of the bit
	public void FlipFlop()
	{
		if (bitMoved == true)
		{
			bitMoved = !bitMoved;
			currentState = GameState.on;
		}
	}

	// This is the function that controls the actions of the bit moving up
	void activate()
	{
		renderer.material = red;
		// Set the message of the bit display
		Display (0);
		transform.position = Vector3.Lerp (transform.position, loweredPosition, Time.deltaTime*4);
	}
	
	// This is the function that controls the actions of the bit moving down
	void deactivate()
	{
		renderer.material = green;
		// Set the message of the bit display
		Display (1);
		transform.position = Vector3.Lerp (transform.position, raisedPosition, Time.deltaTime*4);
	}

	// Main function to change message of the display
	void Display(int value) {
		display.text = value.ToString ();
	}
}
