﻿//***************
// Jeffrey Kuang
// BinaryController.cs
// 3/26/2015
// This script defines the behavior 
// of the binary state machine display
//***************

using UnityEngine;
using System.Collections;

public class BinaryController : MonoBehaviour 
{
	// public class variable delcarations
	public GameObject block0;
	public GameObject block1;
	public GameObject block2;
	public GameObject block3;
	public GUIText message;
	public AudioClip ding;
	public AudioClip tick;

	// Private class variables declarations
	private Vector3 raisedPosition;
	private Vector3 loweredPosition;
	private int value, delta, previous;
	private float StateChanged, ticker;

	// Called at the start of script
	void Start () 
	{
		// Start of the coroutines
		StartCoroutine (Sequence ());
	}

	IEnumerator Sequence()
	{
		// Start sub coroutines of each phases of the display
		Debug.Log ("Phase 1");
		yield return StartCoroutine (initialize ());
		Debug.Log ("Phase 2");
		yield return StartCoroutine (counting ());
		Debug.Log ("Phase 3");
		yield return StartCoroutine (wave ());
		Debug.Log ("Phase 4");
		yield return StartCoroutine (end ());
		yield break;
		}

	IEnumerator initialize()
	{
		// Start by turning on all of the bits in the first second
		switchBit(block0,1);
		switchBit(block1,1);
		switchBit(block2,1);
		switchBit(block3,1);
		yield return new WaitForSeconds(1.0f);

		// Zoom in on the display using lerp
		float startTime = Time.time;
		Vector3 startPosition = transform.position;
		Vector3 targetPosition = new Vector3 (0, 4, -8);
		message.text = "Initializing...";
		while(Vector3.Distance(transform.position, targetPosition ) > .01f) {
			// move toward display
			transform.position = Vector3.Lerp (startPosition, targetPosition, (Time.time-startTime));

			yield return null;
		}
		yield return null;
	}
	
	IEnumerator counting()
	{
		// Two seconds of no counting but start ticking!
		StateChanged = Time.time;
		while (GetStateElapsed() < 2.0f){
			ticker += Time.deltaTime;
			if (ticker > 0.5f){
				audio.PlayOneShot (tick);
				ticker -= 0.5f;
			}
			yield return null;
		}
		// reset timer
		StateChanged = Time.time;
		// play initial ding
		audio.PlayOneShot (ding);

		// Control loop structure to progress through counting
		while(GetStateElapsed() < 16.0f){
			//Debug.Log ("time is "+GetStateElapsed().ToString());
			value = delta = (int)GetStateElapsed();
			message.text = "Counting: "+delta.ToString();

			// timer to play tick at eacy half second
			ticker += Time.deltaTime;
			if (ticker > 0.5f){
				audio.PlayOneShot (tick);
				ticker -= 0.5f;
			}

			// function that indicates the transition to the next counter
			if(previous != delta){
				audio.PlayOneShot (ding);
				Debug.Log (value.ToString());
				previous = delta;
			}

			// 4th bit
			if(value >= 8){
				value -= 8;
				switchBit(block3,1);
				bitMoved(block3);
			}
			else
				switchBit(block3,0);			

			// 3rd bit
			if(value >= 4){
				value -= 4;
				switchBit(block2,1);
				bitMoved(block2);
			}
			else
				switchBit(block2,0);

			// 2nd bit
			if(value >= 2){
				value -= 2;
				switchBit(block1,1);
				bitMoved(block1);
			}
			else
				switchBit(block1,0);

			// 1st bit
			if(value >= 1){
				value -= 1;
				switchBit(block0,1);
				bitMoved(block0);
			}
			else
				switchBit(block0,0);

			yield return null;
		}
		yield return null;
	}

	IEnumerator wave()
	{
		// reset the elpased timer by reassigning current time 
		StateChanged = Time.time;

		// Loop control for progression of the wave
		while(GetStateElapsed() < 9.0f)
		{
			if(audio.isPlaying && audio.time > .75f)
				audio.Stop();
			delta =(int)GetStateElapsed();

			// Indicates changing of the next bit
			// Control structure that adjusts the pitch of the ding
			if(previous != delta){
				if (delta > 1 && delta < 5){
					audio.Play();
					audio.pitch -= .25f;
				}else if(delta == 8 || delta == 0)
					;
				else {
					audio.Play();
					audio.pitch += .25f;
				}
				Debug.Log (value.ToString());
				previous = delta;
			}

			// Initialize all of the bit as off
			if((int)GetStateElapsed() == 0.0f){
				message.text = "Wave!!!";
				switchBit(block0,0);
				switchBit(block1,0);
				switchBit(block2,0);
				switchBit(block3,0);
			}

			// start the wave
			if((int)GetStateElapsed() == 1.0f){
				switchBit(block3,1);
			}
			if((int)GetStateElapsed() == 2.0f){
				switchBit(block2,1);
				if(GetStateElapsed() > 2.5f)
					switchBit(block3,0);
			}
			if((int)GetStateElapsed() == 3.0f){
				switchBit(block1,1);
				if(GetStateElapsed() > 3.5f)
					switchBit(block2,0);
			}

			// reaching the last bit from one direction
			if((int)GetStateElapsed() == 4.0f){
				switchBit(block0,1);
				if(GetStateElapsed() > 4.5f)
					switchBit(block1,0);
			}

			// starting the return of the wave
			if((int)GetStateElapsed() == 5.0f){
				switchBit(block1,1);
				if(GetStateElapsed() > 5.5f)
					switchBit(block0,0);
			}
			if((int)GetStateElapsed() == 6.0f){
				switchBit(block2,1);
				if(GetStateElapsed() > 6.5f)
					switchBit(block1,0);
			}
			if((int)GetStateElapsed() == 7.0f){
				switchBit(block3,1);
				if(GetStateElapsed() > 7.5f)
					switchBit(block2,0);
			}
			if((int)GetStateElapsed() == 8.0f)
				switchBit(block3,0);
			yield return null;
		}
		yield return null;
	}
	
	IEnumerator end()
	{
		message.text = "GoodBye :)";
		yield return null;
	}

	// Switches the bit state to either on or off, but it wouldn't move if the flag is enabled
	void switchBit(GameObject block, int state)
	{
		BitController bit = (BitController) block.GetComponent<BitController>();
		if (state == 1)
			bit.currentState = BitController.GameState.on;
		else
			bit.currentState = BitController.GameState.off;
	}

	// Flag function to notifiy that this bit needs to move
	void bitMoved(GameObject block){
		BitController bit = (BitController) block.GetComponent<BitController>();
		bit.bitMoved = true;
	}

	// Keeps track of the elpased time at the beginning of each coroutine
	float GetStateElapsed(){
		return Time.time - StateChanged;
	}

}