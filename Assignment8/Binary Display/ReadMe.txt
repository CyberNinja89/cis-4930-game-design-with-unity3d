********** Binary Display **********
Name:
  Jeffrey Kuang

Date:
  3/26/2015

Description:
    This project focuses on coroutines and state machines. The main script (BinaryController)
has the main coroutine that controls the phases. Each bit has a state machine script that
enumerates off and on state. The phases are the following: Initialization, Counting, Wave, End.
Each bit changes color to either green or red for the the on and off states. The wave feature 
has a slight delay for the bit going down as specified in the writeup.

Note: I attempted the both parts of the extra credit. I add the the number display on the bit. I
interrepreted the "Top" as being infront of the moving bit. Secondly, I implemented pitch changing
as the wave progress down and back the binary display