﻿using UnityEngine;
using Holoville.HOTween;
using System.Collections;

public class InputController : MonoBehaviour {

	public UISprite spriteLaunch, spritePlanet, spriteSpace;
	public UISprite progFrame, progFill;
	public UIPanel panel, panel2;

	bool isReturnPressed = false;

	void Start () {
		movePlanet (-0.1f, 1.0f);
		setProgressBarVisible (false);
		SetProgressBarValue (0);
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.Return) && !isReturnPressed){
			setLaunch (true);
			StartCoroutine(fillProgressBar());
			isReturnPressed = true;
		}
	}

	IEnumerator fillProgressBar(){
		while(getProgressBarValue() < 1) {
			addProgress(0.01f);
			yield return new WaitForSeconds(0.01f);
		}

		while(panel.alpha > 0) {
			panel.alpha -= 0.01f;
			yield return new WaitForSeconds(0.01f);
		}

		while(panel2.alpha > 0) {
			panel2.alpha -= 0.01f;
			yield return new WaitForSeconds(0.01f);
		}

		yield return null;
	}

	void movePlanet(float delY, float duration){
		HOTween.From (spriteSpace.transform, duration, "position", new Vector3 (0, delY, 0), true);
		delY -= 0.2f;
		HOTween.From (spritePlanet.transform, duration, "position", new Vector3 (0, delY, 0), true);
	}

	void setProgressBarVisible (bool status){
		if (status){
			progFrame.gameObject.SetActive (true);
			progFill.gameObject.SetActive (true);
		} else {
			progFrame.gameObject.SetActive (false);
			progFill.gameObject.SetActive (false);
		}
	}
	
	void setLaunch (bool status){
		if (status){
			spriteLaunch.gameObject.SetActive (false);
			setProgressBarVisible (true);
		}
	}

	float getProgressBarValue(){
		return progFill.fillAmount;
	}

	void SetProgressBarValue (float value) {
		progFill.fillAmount = value;
	}

	void addProgress(float delta) {
		progFill.fillAmount += delta;
	}
}